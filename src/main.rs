mod wrappers;

use mantid_grep::grep_cli;
use mantid_hash::crack_cli;
use mantid_http::http_cli;

use clap::{CommandFactory, Parser, Subcommand};

#[derive(Parser)]
#[command(author, version, about = None, long_about = None, arg_required_else_help(true))]
struct Cli {
    #[command(subcommand)]
    command: Option<Commands>,
}

#[derive(Subcommand)]
enum Commands {
    Hash {
        wordlist: String,
        hash: String,
        #[arg(value_enum)]
        algorithm: Option<wrappers::HashAlgoValue>,
    },
    Grep {
        #[arg(conflicts_with = "hash")]
        target: Option<String>,
        #[arg(short = 'i', long = "ignore-case")]
        case_insensitive: bool,
        #[arg(short = 'o', long = "only-matching")]
        only_matching: bool,
        #[arg(long = "hash", exclusive = true)]
        hash: bool,
    },
    Http {
        uri: String,
        #[arg(long = "headers")]
        headers: bool,
    },
}

fn main() {
    let cli = Cli::parse();

    let res = match &cli.command {
        Some(Commands::Hash {
            wordlist,
            hash,
            algorithm,
        }) => match algorithm {
            Some(hav) => crack_cli(wordlist, hash.trim(), Some(&hav.0)),
            None => crack_cli(wordlist, hash.trim(), None),
        },
        Some(Commands::Grep {
            target,
            case_insensitive,
            only_matching,
            hash,
        }) => match target {
            Some(t) => grep_cli(t, *case_insensitive, *only_matching, *hash),
            None => {
                if *hash {
                    grep_cli("", *case_insensitive, *only_matching, *hash)
                } else {
                    // print subcommand docs
                    let mut c = Cli::command();
                    println!(
                        "{}",
                        c.get_subcommands_mut()
                            .find(|sc| sc.get_name() == "grep")
                            .expect("UNREACHABLE")
                            .render_help()
                    );
                    Ok(())
                }
            }
        },
        Some(Commands::Http { uri, headers }) => http_cli(uri, headers),
        None => Ok(()),
    };

    if res.is_err() {
        eprintln!("{}", res.unwrap_err());
    }
}
