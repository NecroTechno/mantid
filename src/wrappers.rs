use mantid_core::constants;
use mantid_hash::HashAlgo;

use clap::builder::PossibleValue;
use clap::ValueEnum;

#[derive(Clone)]
pub struct HashAlgoValue(pub HashAlgo);

impl ValueEnum for HashAlgoValue {
    fn value_variants<'a>() -> &'a [Self] {
        &[
            HashAlgoValue(HashAlgo::MD5),
            HashAlgoValue(HashAlgo::SHA1),
            HashAlgoValue(HashAlgo::SHA256),
        ]
    }

    fn to_possible_value(&self) -> Option<PossibleValue> {
        match self.0 {
            HashAlgo::MD5 => Some(PossibleValue::new(constants::MD5)),
            HashAlgo::SHA1 => Some(PossibleValue::new(constants::SHA1)),
            HashAlgo::SHA256 => Some(PossibleValue::new(constants::SHA256)),
        }
    }
}
