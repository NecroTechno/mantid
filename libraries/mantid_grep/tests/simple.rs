use mantid_grep::{matches, MatchesInput};

#[test]
fn test_simple() {
    let res = matches("test", MatchesInput::Expression("TE"), true).unwrap();
    assert_eq!(res[0].as_str(), "te");
}
