use regex::Error as RegexError;
use std::error;
use std::fmt;
use std::io::Error as IOError;

#[derive(Debug)]
pub enum Error {
    InvalidSlice,
    IO(IOError),
    Regex(RegexError),
    HashRetrieve,
}

impl fmt::Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match *self {
            Error::InvalidSlice => write!(f, "invalid slice"),
            Error::IO(..) => write!(f, "failed to read stdin"),
            Error::Regex(..) => write!(f, "failed to parse regex"),
            Error::HashRetrieve => write!(f, "failed to retrieve hashmap value"),
        }
    }
}

impl error::Error for Error {
    fn source(&self) -> Option<&(dyn error::Error + 'static)> {
        match *self {
            Error::InvalidSlice => None,
            Error::IO(ref e) => Some(e),
            Error::Regex(ref e) => Some(e),
            Error::HashRetrieve => None,
        }
    }
}

impl From<IOError> for Error {
    fn from(err: IOError) -> Error {
        Error::IO(err)
    }
}

impl From<RegexError> for Error {
    fn from(err: RegexError) -> Error {
        Error::Regex(err)
    }
}
