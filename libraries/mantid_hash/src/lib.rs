mod error;

use error::Error;
use mantid_core::{constants, Boxed};
use mantid_grep::matches;

use sha1::Digest;
use std::{
    fs::File,
    io::{BufRead, BufReader},
};

#[derive(Clone)]
pub enum HashAlgo {
    MD5,
    SHA1,
    SHA256,
}

// impl From<&str> for HashAlgo {
//     fn from(s: &'a str) -> Option<Self> {
//         for key in constants::HASH_EXP.keys() {
//             match s {
//                 "md5" => Some(HashAlgo::MD5),
//                 "sha1" => Some(HashAlgo::SHA1),
//                 "sha256" => Some(HashAlgo::SHA256),
//                 _ => None
//             }
//         }
//     }
// }

// currently only handles sha-1 hashes
pub fn crack(
    wordlist_file_path: &str,
    hash_to_crack: &str,
    algo: &HashAlgo,
) -> Result<Option<String>, Error> {
    let wordlist_file = File::open(wordlist_file_path)?;
    let reader = BufReader::new(&wordlist_file);

    for line in reader.lines() {
        let line = line?;
        let common_password = line.trim();
        match algo {
            HashAlgo::SHA1 => {
                if hash_to_crack == hex::encode(sha1::Sha1::digest(common_password.as_bytes())) {
                    return Ok(Some(common_password.to_string()));
                }
            }

            _ => {
                println!("Algorithm not currently implemented");
                return Ok(None);
            }
        }
    }

    Ok(None)
}

pub fn crack_cli(wordlist_file_path: &str, hash_to_crack: &str, _algo: Option<&HashAlgo>) -> Boxed {
    // let mut possible_algo: Vec<HashAlgo> = Vec::new();

    // for (k,v) in constants.HASH_EXP.iter() {
    //     if !grep_cli::matches(hash_to_crack, MatchesInput::Expression(v), true).is_empty() {
    //         possible_algo.push()
    //     }
    // }

    let res = crack(wordlist_file_path, hash_to_crack, &HashAlgo::SHA1)?;
    match res {
        Some(pass) => println!("Password found: {}", &pass),
        None => println!("Password not found in wordlist."),
    };

    Ok(())
}
