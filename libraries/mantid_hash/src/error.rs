use std::error;
use std::fmt;
use std::io::Error as IOError;

#[derive(Debug)]
pub enum Error {
    InvalidHash,
    IO(IOError),
}

impl fmt::Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match *self {
            Error::InvalidHash => write!(f, "hash is not valid"),
            Error::IO(..) => write!(f, "failed to read the wordlist"),
        }
    }
}

impl error::Error for Error {
    fn source(&self) -> Option<&(dyn error::Error + 'static)> {
        match *self {
            Error::InvalidHash => None,
            Error::IO(ref e) => Some(e),
        }
    }
}

impl From<IOError> for Error {
    fn from(err: IOError) -> Error {
        Error::IO(err)
    }
}
