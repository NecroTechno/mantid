use mantid_hash::{crack, HashAlgo};

use std::path::PathBuf;

#[test]
fn test_simple() {
    let mut d = PathBuf::from(env!("CARGO_MANIFEST_DIR"));
    d.push("tests/wordlist.txt");
    assert_eq!(
        crack(
            d.to_str().unwrap(),
            "7c6a61c68ef8b9b6b061b28c348bc1ed7921cb53",
            &HashAlgo::SHA1
        )
        .unwrap(),
        Some("passw0rd".to_string())
    );
}
