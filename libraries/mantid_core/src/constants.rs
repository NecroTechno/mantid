use lazy_static::lazy_static;

use std::collections::HashMap;

pub static MD5: &'static str = "md5";
pub static SHA1: &'static str = "sha1";
pub static SHA256: &'static str = "sha256";

lazy_static! {
    pub static ref HASH_EXP: HashMap<&'static str, &'static str> = {
        let mut m = HashMap::new();
        m.insert(MD5, "[0-9a-f]{32}");
        m.insert(SHA1, "[0-9a-f]{40}");
        m.insert(SHA256, "[A-Fa-f0-9]{64}");
        m
    };
}
