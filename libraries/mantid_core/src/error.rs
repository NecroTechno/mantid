use std::error;
use std::fmt;

#[derive(Debug)]
pub enum Error {
    Source,
}

impl fmt::Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match *self {
            Error::Source => write!(f, "unable to find error source"),
        }
    }
}

impl error::Error for Error {
    fn source(&self) -> Option<&(dyn error::Error + 'static)> {
        match *self {
            Error::Source => None,
        }
    }
}
