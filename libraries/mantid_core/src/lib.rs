pub mod constants;
pub mod error;

pub type Boxed = std::result::Result<(), Box<dyn std::error::Error>>;
