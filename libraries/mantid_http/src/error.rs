use std::error;
use std::error::Error as StdError;
use std::fmt;

use mantid_core::error::Error as MantidError;
use reqwest::Error as ReqwestError;
use reqwest::header::ToStrError;

#[derive(Debug)]
pub enum Error {
    Reqwest(ReqwestError),
    ToStr(ToStrError)
}

impl fmt::Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match *self {
            Error::Reqwest(..) => write!(
                f,
                "Reqwest error: {}",
                self.source().unwrap_or(&MantidError::Source)
            ),
            Error::ToStr(..) => write!(
                f,
                "ToStr error: {}",
                self.source().unwrap_or(&MantidError::Source)
            ),
        }
    }
}

impl error::Error for Error {
    fn source(&self) -> Option<&(dyn error::Error + 'static)> {
        match *self {
            Error::Reqwest(ref e) => Some(e),
            Error::ToStr(ref e) => Some(e),
        }
    }
}

impl From<ReqwestError> for Error {
    fn from(err: ReqwestError) -> Error {
        Error::Reqwest(err)
    }
}

impl From<ToStrError> for Error {
    fn from(err: ToStrError) -> Error {
        Error::ToStr(err)
    }
}