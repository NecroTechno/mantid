mod error;

use error::Error;
use mantid_core::Boxed;

use reqwest::Client;
use tokio::runtime::Runtime;

pub async fn get(uri: &str, headers: &bool) -> Result<(), Error> {
    let client = Client::new();
    let res = client.get(uri).send().await?;
    if *headers {
        for (key, value) in res.headers().iter() {
            println!("{}: {}", key, value.to_str()?);
        }
    } else {
        print!("{}", res.text().await?);
    }
    Ok(())
}

pub fn http_cli(uri: &str, headers: &bool) -> Boxed {
    let rt = Runtime::new()?;
    Ok(rt.block_on(get(uri, headers))?)
}
